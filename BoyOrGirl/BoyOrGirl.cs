using System;
using System.Collections.Generic;
using System.Linq;

public class BoyOrGirl
{
    public static void Main()
    {
        string input = Console.ReadLine();

        List<char> letters = input.Distinct().ToList();

        int lettersCount = letters.Count();

        if ( lettersCount % 2 == 0 )
        {
            
            System.Console.WriteLine("CHAT WITH HER!");
        }
        else
        {
            System.Console.WriteLine("IGNORE HIM!");
        }
    }
}