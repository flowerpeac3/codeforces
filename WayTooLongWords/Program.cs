﻿using System;

namespace WayTooLongWords
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());

            for (int i = 0; i < number; i++)
            {
                string str = Console.ReadLine();
                string print = Process(str);
                Console.WriteLine($"{print}");
            }
        }

        private static string Process(string input)
        {
            string result = "";

            if (input.Length <= 10)
            {
                result = input;
                return result;
            }
            else
            {
                string middle = input.Substring(1, input.Length - 2);
                result += input[0];
                result += middle.Length.ToString();
                result += input[input.Length - 1];
            }

            return result;
        }
    }
}
