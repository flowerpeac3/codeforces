﻿using System;
using System.Linq;

namespace BeautifulMatrix
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[,] matrix = new int[5, 5];

            for (int i = 0; i < 5; i++)
            {
                var readline = Console.ReadLine()
                    .Split()
                    .Select(int.Parse)
                    .ToArray();

                for (int j = 0; j < 5; j++)
                {
                    matrix[i, j] = readline[j];
                }
            }

            Console.WriteLine($"{Process(matrix)}");
        }

        private static int[,] AllocateMatrix()
        {
            int[,] matrix = new int[5,5];

            for (int i = 0; i < 5; i++)
            {
                var readline = Console.ReadLine()
                    .Split()
                    .Select(int.Parse)
                    .ToArray();

                for (int j = 0; j < 5; j++)
                {
                    matrix[i, j] = readline[j];
                }
            }

            return matrix;
        }

        private static int Process(int[,] input)
        {
            int moveRight = 0;
            int moveDown = 0;

            int locateRow = LocateRow(input);

            int locateCol = LocateCol(input);

           if (locateRow > 2)
            {
                for (int i = locateRow + 1; i < input.GetLength(0); i++)
                    moveDown++;

                moveDown += 2;
            }
            else
                for (int i = locateRow; i < 2; i++)
                    moveDown++;
            

            if (locateCol > 2)
            {
                for (int i = locateCol + 1; i < input.GetLength(1); i++)
                    moveRight++;

                moveRight += 2;
            }
            else
                for (int i = locateCol; i < 2; i++)
                    moveRight++;
            

            if (locateRow == 2)
                moveDown = 0;

            if (locateCol == 2)
                moveRight = 0;

            return moveDown + moveRight;
        }

        private static int LocateRow(int[,] input)
        {
            int n = 0;

            for (int i = 0; i < input.GetLength(0); i++)
                for (int j = 0; j < input.GetLength(1); j++)
                    if (input[i, j] == 1)
                        n = i;

            return n;
        }
        private static int LocateCol(int[,] input)
        {
            int n = 0;

            for (int i = 0; i < input.GetLength(0); i++)
                for (int j = 0; j < input.GetLength(1); j++)
                    if (input[i, j] == 1)
                        n = j;

            return n;
        }
    }
}
