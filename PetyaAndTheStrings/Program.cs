﻿using System;

namespace PetyaAndTheStrings
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string input1 = Console.ReadLine();
            string input2 = Console.ReadLine();

            int result = Process(input1, input2);

            Console.WriteLine(result);
        }

        private static int Process(string input1, string input2)
        {
            string str1 = input1.ToLower();
            string str2 = input2.ToLower();

            int result = 0;

            for (int i = 0; i < str1.Length; i++)
            {
                char c1 = str1[i];

                if (c1 > (int)str2[i])
                {
                    result = 1;
                    break;
                }
                else if (c1 < (int)str2[i])
                {
                    result = -1;
                    break;
                }
                else
                {
                    result = 0;
                }
            }

            return result;
        }
    }
}
