﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace A.Team
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());

            int count = 0;

            for (int i = 0; i < number; i++)
            {
                var opinions = Console.ReadLine()
                    .Split()
                    .Select(int.Parse)
                    .ToList(); 

                if (Process(opinions))
                {
                    count++;
                }
            }

            Console.WriteLine(count);
        }

        private static bool Process(IEnumerable<int> input)
        {
            var list = input.ToList();

            if (list[0] == 1 && list[1] == 1)
            {
                return true;
            }
            else if (list[1] == 1 && list[2] == 1)
            {
                return true;
            }
            else if (list[0] == 1 && list[2] == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}