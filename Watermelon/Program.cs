﻿using System;

namespace Watermelon
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int input = int.Parse(Console.ReadLine());

            
            if (input % 2 == 0 && input > 2)
            {
                Console.WriteLine("YES");
            }
            else
            {
                double precisionWeight = input;
                double partOne = precisionWeight / 2;
                double partTwo = precisionWeight / 2;
                partOne += 0.1D;
                partTwo -= 0.1D;
                Console.WriteLine("NO");
            }
        }
    }
}
